package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class TagRepositoryImpl implements TagRepository {
    @Override
    public List<Tag> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Tag> foundList = em.createNativeQuery
                ("SELECT * FROM tag", Tag.class).getResultList();

        em.close();

        System.out.println("Found " + foundList.size() + " tag");
        return foundList;
    }

    @Override
    public Tag findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Tag foundCollection = em.find(Tag.class, id);

        em.close();
        System.out.println("Found Tag: " + foundCollection);
        return foundCollection;
    }

    @Override
    public Tag create(Tag tag) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(tag);

        em.getTransaction().commit();
        em.close();
        System.out.println("Tag was create. Id " + tag.getId());
        return tag;
    }

    @Override
    public Tag update(Tag tag) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(tag);

        em.getTransaction().commit();
        em.close();
        System.out.println("Tag was update. Id " + tag.getId());
        return tag;
    }

    @Override
    public void deleteById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Tag foundCollection = em.find(Tag.class, id);
        em.remove(foundCollection);

        em.getTransaction().commit();
        em.close();
    }
}
