package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import java.util.List;

public class CollectionRepositoryImpl implements CollectionRepository {

    @Override
    public List<Collection> findAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Collection> foundList = em.createNativeQuery
                ("SELECT * FROM collection", Collection.class).getResultList();

        em.close();

        System.out.println("Found " + foundList.size() + " collections");
        return foundList;
    }

    @Override
    public Collection findById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        Collection foundCollection = em.find(Collection.class, id);
        // foundCollection.getItem().size();
        Hibernate.initialize(foundCollection.getItem());

        em.close();
        System.out.println("Found collection: " + foundCollection);
        return foundCollection;
    }

    @Override
    public Collection create(Collection collection) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(collection);

        em.getTransaction().commit();
        em.close();
        System.out.println("Collection was create. Id " + collection.getId());
        return collection;
    }

    @Override
    public Collection update(Collection collection) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(collection);

        em.getTransaction().commit();
        em.close();
        System.out.println("Collection was update. Id " + collection.getId());
        return collection;
    }

    @Override
    public void deleteById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Collection foundCollection = em.find(Collection.class, id);
        em.remove(foundCollection);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collection").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }


}
