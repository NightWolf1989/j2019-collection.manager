package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class CommentRepositoryImpl implements CommentRepository {
    @Override
    public List<Comment> findAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Comment> foundList = em.createNativeQuery
                ("SELECT * FROM comment", Comment.class).getResultList();

        em.close();

        System.out.println("Found " + foundList.size() + " comment");
        return null;
    }

    @Override
    public Comment findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Comment foundCollection = em.find(Comment.class, id);

        em.close();
        System.out.println("Found comment: " + foundCollection);
        return foundCollection;
    }

    @Override
    public Comment create(Comment comment) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(comment);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment was create. Id " + comment.getId());
        return comment;
    }

    @Override
    public Comment update(Comment comment) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(comment);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment was update. Id " + comment.getId());
        return comment;
    }

    @Override
    public void deleteById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Comment foundCollection = em.find(Comment.class, id);
        em.remove(foundCollection);

        em.getTransaction().commit();
        em.close();
    }
}
