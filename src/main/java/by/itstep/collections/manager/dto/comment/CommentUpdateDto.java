package by.itstep.collections.manager.dto.comment;

import lombok.Data;

import java.sql.Date;

@Data
public class CommentUpdateDto {

    private Long id;
    private String message;

}
