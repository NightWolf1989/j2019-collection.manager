package by.itstep.collections.manager.dto.comment;

import lombok.Data;


@Data
public class CommentCreateDto {

    private String message;
    private Long userId; // Тот кто его оставил
    private Long collectionId; // Под кем комемент (К чему относится)

}
