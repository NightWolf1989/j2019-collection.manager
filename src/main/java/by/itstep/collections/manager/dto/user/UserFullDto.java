package by.itstep.collections.manager.dto.user;

import by.itstep.collections.manager.Enum.Role;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Comment;
import lombok.Data;

import java.util.List;

@Data
public class UserFullDto {


    private Long id;
    private String Name;
    private String lastName;
    private String email;
    private List<Comment> comments;
    private List<Collection> collections;
    private Role role;
}
