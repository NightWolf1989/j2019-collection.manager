package by.itstep.collections.manager.dto.comment;

import lombok.Data;

import java.sql.Date;

@Data
public class CommentPreviewDto {

    private Long id;
    private String message;
    private String userName; // Тот кто его оставил
    private String collectionName; // Под кем комемент (К чему относится)
    private Date createdAt; // дата создания коммента
}
