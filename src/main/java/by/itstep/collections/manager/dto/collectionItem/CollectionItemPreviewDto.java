package by.itstep.collections.manager.dto.collectionItem;

import lombok.Data;

@Data
public class CollectionItemPreviewDto {

    private Long id;
    private String name;
}
