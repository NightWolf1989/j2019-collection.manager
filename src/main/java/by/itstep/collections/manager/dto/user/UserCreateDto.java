package by.itstep.collections.manager.dto.user;

import lombok.Data;

@Data
public class UserCreateDto {

    private String name;
    private String lastName;
    private String email;
    private String password;

}
