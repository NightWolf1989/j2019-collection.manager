package by.itstep.collections.manager.dto.comment;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import lombok.Data;

import java.sql.Date;

@Data
public class CommentFullDto {

    private Long id;
    private String message;
    private User user; // Тот кто его оставил
    private Collection collection; // Под кем комемент (К чему относится)
    private Date createdAt; // дата создания коммента
}
