package by.itstep.collections.manager.dto.collectionItem;

import lombok.Data;

@Data
public class CollectionItemCreateDto {

    private String name;
}
