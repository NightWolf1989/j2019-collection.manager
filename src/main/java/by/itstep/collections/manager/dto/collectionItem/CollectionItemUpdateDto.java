package by.itstep.collections.manager.dto.collectionItem;

import lombok.Data;

@Data
public class CollectionItemUpdateDto {

    private Long id;
    private String name;
}
