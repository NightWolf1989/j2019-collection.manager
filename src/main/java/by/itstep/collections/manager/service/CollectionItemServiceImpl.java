package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collectionItem.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemFullDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.mapper.CollectionItemMapper;
import by.itstep.collections.manager.repository.CollectionItemRepository;
import by.itstep.collections.manager.repository.CollectionItemRepositoryImpl;

import java.util.List;

public class CollectionItemServiceImpl implements CollectionItemService {

    private CollectionItemRepository collectionItemRepository = new CollectionItemRepositoryImpl();
    private CollectionItemMapper mapper = new CollectionItemMapper();

    @Override
    public List<CollectionItemPreviewDto> findAll() {

        List<CollectionItem> found = collectionItemRepository.findAll();

        return mapper.mapToDtoList(found);
    }

    @Override
    public CollectionItemFullDto findById(Long id) {
        CollectionItem found = collectionItemRepository.findById(id);

        CollectionItemFullDto response = mapper.mapToDto(found);
        return response;
    }

    @Override
    public CollectionItemFullDto create(CollectionItemCreateDto createDto) {
        CollectionItem toSave = mapper.mapToEntity(createDto);

        CollectionItem create = collectionItemRepository.create(toSave);

        CollectionItemFullDto response = mapper.mapToDto(create);
        return response;
    }

    @Override
    public CollectionItemFullDto update(CollectionItemUpdateDto updateDto) {

        CollectionItem entityUpdate = mapper.mapToEntity(updateDto);
        CollectionItem existingEntity = collectionItemRepository.findById(updateDto.getId());

        entityUpdate.setName(existingEntity.getName());

        CollectionItem create = collectionItemRepository.update(entityUpdate);

        CollectionItemFullDto response = mapper.mapToDto(create);
        return response;
    }

    @Override
    public void deleteById(Long id) {
       collectionItemRepository.findById(id);
    }
}
