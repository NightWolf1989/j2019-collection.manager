package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.comment.CommentCreateDto;
import by.itstep.collections.manager.dto.comment.CommentFullDto;
import by.itstep.collections.manager.dto.comment.CommentPreviewDto;
import by.itstep.collections.manager.dto.comment.CommentUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.mapper.CommentMapper;
import by.itstep.collections.manager.repository.*;

import java.util.List;

public class CommentServiceImpl implements CommentService {

    private CommentRepository commentRepository = new CommentRepositoryImpl();
    private CommentMapper mapper = new CommentMapper();
    private UserRepository userRepository = new UserRepositoryImpl();
    private CollectionRepository collectionRepository = new CollectionRepositoryImpl();

    @Override
    public List<CommentPreviewDto> findAll() {
        List<Comment> found = commentRepository.findAll();

        return mapper.mapToDtoList(found);
    }

    @Override
    public CommentFullDto findById(Long id) {
        Comment found = commentRepository.findById(id);

        CommentFullDto response = mapper.mapToDto(found);
        return null;
    }

    @Override
    public CommentFullDto create(CommentCreateDto createDto) {
        User user = userRepository.findById(createDto.getUserId());
        Collection collection = collectionRepository.findById(createDto.getCollectionId());

        Comment toSave = mapper.mapToEntity(createDto, user, collection);

        Comment comment = commentRepository.create(toSave);
        CommentFullDto response = mapper.mapToDto(comment);

        return response;
    }

    @Override
    public CommentFullDto update(CommentUpdateDto updateDto) {
        Comment entityToUpdate = mapper.mapToEntity(updateDto);
        Comment existingEntity = commentRepository.findById(updateDto.getId());

        entityToUpdate.setCollection(existingEntity.getCollection());
        entityToUpdate.setUser(existingEntity.getUser());

        Comment create = commentRepository.update(entityToUpdate);

        CommentFullDto response = mapper.mapToDto(create);
        return response;
    }

    @Override
    public void deleteById(Long id) {
        commentRepository.deleteById(id);
    }
}
