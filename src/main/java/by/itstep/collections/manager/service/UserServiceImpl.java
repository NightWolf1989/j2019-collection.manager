package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserPreviewDto;
import by.itstep.collections.manager.dto.user.UserUpdateDto;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.mapper.UserMapper;
import by.itstep.collections.manager.repository.UserRepository;
import by.itstep.collections.manager.repository.UserRepositoryImpl;

import java.util.List;

public class UserServiceImpl implements UserService {

    private UserRepository userRepository = new UserRepositoryImpl();
    private UserMapper mapper = new UserMapper();

    @Override
    public List<UserPreviewDto> findAll() {

        List<User> found = userRepository.findAll();

        return mapper.mapToDoList(found);
    }

    @Override
    public UserFullDto findById(Long id) {
        User found = userRepository.findById(id);

        return mapper.mapToDto(found);
    }

    @Override
    public UserFullDto create(UserCreateDto createDto) {
        User toSave = mapper.mapToEntity(createDto);

        User create = userRepository.create(toSave);


        UserFullDto response = mapper.mapToDto(create);

        return response;
    }

    @Override
    public UserFullDto update(UserUpdateDto updateDto) {
        User entityUpdate = mapper.mapToEntity(updateDto);
        User existingEntity = userRepository.findById(updateDto.getId());

        entityUpdate.setCollections(existingEntity.getCollections());
        entityUpdate.setComments(existingEntity.getComments());
        entityUpdate.setRole(existingEntity.getRole());

        User create = userRepository.update(entityUpdate);

        UserFullDto fullDto = mapper.mapToDto(create);
        return fullDto;
    }


    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }
}
