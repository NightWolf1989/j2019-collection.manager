package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.exception.InvalidDtoException;
import by.itstep.collections.manager.exception.MissedUpdateIdException;
import by.itstep.collections.manager.mapper.CollectionMapper;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.CollectionRepositoryImpl;
import by.itstep.collections.manager.repository.UserRepository;
import by.itstep.collections.manager.repository.UserRepositoryImpl;

import java.util.List;

public class CollectionServiceImpl implements CollectionService {

    private CollectionRepository collectionRepository = new CollectionRepositoryImpl();
    private CollectionMapper mapper = new CollectionMapper();
    private UserRepository userRepository = new UserRepositoryImpl();

    @Override
    public List<CollectionPreviewDto> findAll() {
        List<Collection> found = collectionRepository.findAll();
        System.out.println("CollectionServiceImpl -> found " + found.size() + " collection");
//        List<CollectionPreviewDto> converted = mapper.mapToDtoList(found);
//        return converted;
        return mapper.mapToDtoList(found);
    }

    @Override
    public CollectionFullDto findById(Long id) {
        Collection found = collectionRepository.findById(id);
        System.out.println("CollectionServiceImpl -> found " + found);

        CollectionFullDto response = mapper.mapToDto(found);
        return response;
    }

    @Override
    public CollectionFullDto create(CollectionCreateDto createDto) throws InvalidDtoException {
        validateCreateDto(createDto);
        User user = userRepository.findById(createDto.getUserId());
        Collection toSave = mapper.mapToEntity(createDto, user );


        Collection created = collectionRepository.create(toSave);
        System.out.println("CollectionServiceImpl -> create collection " + created);

        CollectionFullDto response = mapper.mapToDto(created);
        return response;
    }

    @Override
    public CollectionFullDto update(CollectionUpdateDto updateDto) throws MissedUpdateIdException {
        validateUpdateDto(updateDto);
        Collection entityToUpdate = mapper.mapToEntity(updateDto);
        Collection existingEntity = collectionRepository.findById(updateDto.getId());

        entityToUpdate.setImageUrl(existingEntity.getImageUrl());
        entityToUpdate.setUser(existingEntity.getUser());

        Collection create = collectionRepository.update(entityToUpdate);
        System.out.println("CollectionServiceImpl -> update collection " + create);

        CollectionFullDto response = mapper.mapToDto(create);
        return response;
    }

    @Override
    public void deleteById(Long id) {
        collectionRepository.deleteById(id);
        System.out.println("CollectionServiceImpl -> collection with id " + id + " was deleted");
    }

    private void validateCreateDto(CollectionCreateDto dto) throws InvalidDtoException {
        if (dto.getUserId() == null || dto.getDescription() == null ||
            dto.getName() == null||  dto.getTitle() == null){
            throw new InvalidDtoException("One or more fields is null");
        }
        if (dto.getDescription().length() < 15){
            throw new InvalidDtoException("Description min length is 15");
        }
    }

    private void validateUpdateDto(CollectionUpdateDto dto) throws MissedUpdateIdException {
        if (dto.getId() == null){
            throw new MissedUpdateIdException("Collection id is not specified");
        }
    }
}
