package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.user.UserLoginDto;

public interface AuthService {

    void login(UserLoginDto loginDto);

    boolean isAuthenticated(String email);

    void logOut(String email);

    Long getAuthenticatedId(String email);
}
