package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemFullDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.collectionItem.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.CollectionItem;

import java.util.ArrayList;
import java.util.List;

public class CollectionItemMapper {

    public List<CollectionItemPreviewDto> mapToDtoList (List<CollectionItem> entities){
        List<CollectionItemPreviewDto> dtos = new ArrayList<>();

        for (CollectionItem entity : entities){

            CollectionItemPreviewDto dto = new CollectionItemPreviewDto();

            dto.setName(entity.getName());

            dtos.add(dto);
        }

        return dtos;
    }

    public CollectionItem mapToEntity (CollectionItemCreateDto createDto){
        CollectionItem collectionItem = new CollectionItem();

        collectionItem.setName(createDto.getName());

        return collectionItem;
    }

    public CollectionItem mapToEntity(CollectionItemUpdateDto updateDto){
        CollectionItem collectionItem = new CollectionItem();
        collectionItem.setId(updateDto.getId());
        collectionItem.setName(updateDto.getName());

        return collectionItem;

    }

    public CollectionItemFullDto mapToDto(CollectionItem entity){
        CollectionItemFullDto fullDto = new CollectionItemFullDto();
        //...
        return  fullDto;
    }
}
