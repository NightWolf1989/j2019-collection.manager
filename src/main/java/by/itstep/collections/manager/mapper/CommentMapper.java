package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.comment.CommentCreateDto;
import by.itstep.collections.manager.dto.comment.CommentFullDto;
import by.itstep.collections.manager.dto.comment.CommentPreviewDto;
import by.itstep.collections.manager.dto.comment.CommentUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.User;

import java.util.ArrayList;
import java.util.List;

public class CommentMapper {

    java.util.Date utilDate = new java.util.Date();

    public List<CommentPreviewDto> mapToDtoList (List<Comment> entities){
        List<CommentPreviewDto> dtos = new ArrayList<>();


        for (Comment entity : entities){

            CommentPreviewDto dto = new CommentPreviewDto();

            dto.setId(entity.getId());
            dto.setCreatedAt(entity.getCreatedAt());
            dto.setMessage(entity.getMessage());
            dto.setUserName(entity.getUser().getName() + " " + entity.getUser().getLastName());
            dto.setCollectionName(entity.getCollection().getName());

            dtos.add(dto);
        }

        return dtos;
    }

    public Comment mapToEntity(CommentCreateDto createDto, User user, Collection collection){
        Comment comment = new Comment();

        comment.setMessage(createDto.getMessage());
        comment.setCreatedAt(new java.sql.Date(utilDate.getTime()));
        comment.setUser(user);
        comment.setCollection(collection);

        return comment;
    }

    public Comment mapToEntity(CommentUpdateDto updateDto){
        Comment comment = new Comment();
        comment.setId(updateDto.getId());
        comment.setMessage(updateDto.getMessage());
        comment.setCreatedAt(new java.sql.Date(utilDate.getTime()));

        return comment;
    }

    public CommentFullDto mapToDto(Comment entity){
        CommentFullDto fullDto = new CommentFullDto();
        //
        return fullDto;
    }
}
