package by.itstep.collections.manager.mapper;


import by.itstep.collections.manager.Enum.Role;
import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserPreviewDto;
import by.itstep.collections.manager.dto.user.UserUpdateDto;
import by.itstep.collections.manager.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserMapper {

    public List<UserPreviewDto> mapToDoList(List<User> entities){
        List<UserPreviewDto> dtos = new ArrayList<>();
        for (User entity : entities){

            UserPreviewDto dto = new UserPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setLastName(entity.getLastName());
            dto.setEmail(entity.getEmail());

            dtos.add(dto);
        }

        return dtos;
    }

    public User mapToEntity(UserCreateDto createDto){
        User user = new User();
        user.setName(createDto.getName());
        user.setLastName(createDto.getLastName());
        user.setEmail(createDto.getEmail());
        user.setPassword(createDto.getPassword());
        user.setRole(Role.USER);


        return user;
    }

    public User mapToEntity(UserUpdateDto updateDto){
        User user = new User();
        user.setName(updateDto.getName());
        user.setLastName(updateDto.getLastName());
        user.setEmail(updateDto.getEmail());

        return user;

    }

    public UserFullDto mapToDto(User entity){
        UserFullDto fullDto = new UserFullDto();
        fullDto.setId(entity.getId());
        fullDto.setRole(entity.getRole());
        fullDto.setName(entity.getName());
        fullDto.setLastName(entity.getLastName());
        entity.setCollections(entity.getCollections());
        entity.setComments(entity.getComments());
        return fullDto;

    }
}
