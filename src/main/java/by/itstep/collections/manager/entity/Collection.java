package by.itstep.collections.manager.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "collection")
public class Collection {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", nullable = false) // nullable = false :имя должно быть указано обязательно!
  private String name;

  @Column(name = "title")
  private String title;

  @Column(name = "image_url")
  private String imageUrl;

  @Column(name = "description")
  private String description;

  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  @OneToMany(mappedBy = "collection") // коллекция одна, а у нее много item' ов
  private List<CollectionItem> item;

  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  @OneToMany(mappedBy = "collection")
  private List<Comment> comments;

  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;

  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  @ManyToMany
  @JoinTable(
          name = "collection_tag",
          joinColumns = {@JoinColumn(name = "collection_id")},
          inverseJoinColumns = {@JoinColumn(name = "tag_id")}
          )
  private List<Tag> tags;

}
