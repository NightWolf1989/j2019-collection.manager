package by.itstep.collections.manager.entity;

import com.sun.xml.bind.v2.TODO;
import lombok.*;


import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "comment")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "message")
    private String message;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user; // Тот кто его оставил

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn(name = "collection_id")
    private Collection collection; // Под кем комемент (К чему относится)

    @Column(name = "create_at")
    private Date createdAt; // дата создания коммента
}
