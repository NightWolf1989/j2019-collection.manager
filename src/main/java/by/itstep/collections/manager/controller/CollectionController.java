package by.itstep.collections.manager.controller;

import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserLoginDto;
import by.itstep.collections.manager.service.CollectionService;
import by.itstep.collections.manager.service.CollectionServiceImpl;
import by.itstep.collections.manager.service.UserService;
import by.itstep.collections.manager.service.UserServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class CollectionController {
    /*
    API

    1. Получить главную страницу
    2. Получить страницу по id колекции
    3. Получить страницу с формой создания колекции
    4. Дать возможность создать колекцию из формы
    5. Удолять колекции
    6. Создавать колекции

    */

    private CollectionService collectionService = new CollectionServiceImpl();
    private UserService userService = new UserServiceImpl();

    @RequestMapping(method = RequestMethod.GET, value = "/index")
    public String openMainPage(Model model){
//        List<CollectionPreviewDto>  found1 = collectionService.findAll();
        List<CollectionPreviewDto> found = generateAll();
        List<String> strings = Arrays.asList("A", "B", "C");

        model.addAttribute("value", "Hello from Thymeleaf");

        model.addAttribute("all_collections", found);
        model.addAttribute("all_string", strings);
        return "index"; // Thymeleaf -> resources/templates/index.html
    }

    @RequestMapping(method = RequestMethod.GET, value = "/profile/{id}")
    public String openProfile(@PathVariable Long id, Model model){
        UserFullDto found = userService.findById(id);
        model.addAttribute("user", found);

        return "profile";
    }

    private List<CollectionPreviewDto> generateAll(){
        List<CollectionPreviewDto> previews = new ArrayList<>();
        previews.add(new CollectionPreviewDto(1L, "Super cool","title 1", "img.url",
                "Bob Bobson", null));
        previews.add(new CollectionPreviewDto(2L, "Super cool","title 2", "img.url",
                "Bobbie Bobson", null));
        previews.add(new CollectionPreviewDto(3L, "Super cool","title 3", "img.url",
                "Bob Bobson", null));
        previews.add(new CollectionPreviewDto(4L, "Super cool","title 4", "img.url",
                "Bob Bobson", null));
        previews.add(new CollectionPreviewDto(5L, "Super cool","title 5", "img.url",
                "Bob", null));

        return previews;
    }

    //нужен чтобы открыть форму!!!!
    @RequestMapping(method = RequestMethod.GET, value = "/registration")
    public String openRegistrationPage(Model model){
        model.addAttribute("createDto", new UserCreateDto());
        model.addAttribute("loginDto", new UserLoginDto());
        return "sing_in";

    }

    //нужен чтобы зарегистрировать и принять форму на сохранения
    @RequestMapping(method = RequestMethod.POST, value = "/users/create")
    public String saveUser(@ModelAttribute UserCreateDto createDto){
        System.out.println("Что пришло: " + createDto);
        UserFullDto created = userService.create(createDto);
        System.out.println(created);
        return "redirect:/profile/" + created.getId();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/user/login")
    public String login(UserLoginDto loginDto){
        //TODO
        return "redirect:/profile/" + 0;
    }
}
